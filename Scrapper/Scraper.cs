﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Services;
using DAL.Entities;
using System.Text.RegularExpressions;
using System.Net.Http;
using HtmlAgilityPack;

namespace Scrapper
{
    public class Scraper
    {
        public float GetTotalScore(string sentence, List<string> KWList)
        {
            float TotalScore = 0;

            // ------------------------------------------

            int Iscore = 0;
            float Fscore = 0;

            // modify title for special characters
            // ...................................
            string str = sentence;
            string newsentence = "";
            char newchar;

            for (int i = 0; i < str.Length; i++)
            {
                // Console.WriteLine(Char.IsLetterOrDigit(str, i));
                newchar = str[i];
                //Console.WriteLine(str[i]);
                if (Char.IsLetterOrDigit(str, i) != true) { newchar = ' '; newsentence += newchar; };
                newsentence += newchar;
                // Console.WriteLine(newsentence);
            }
            Console.WriteLine(newsentence);

            // Proceed to the scoring
            // ......................
            // Keywords toto = new Keywords();

            int imax = KWList.Count;
            foreach (var kw in KWList)
            {
                Iscore += NumScore(newsentence, kw);
                
            }
            Fscore = 100 * (float)Iscore / KWList.Count();
            Console.WriteLine("KList.Count() = {0} ", KWList.Count());
            Console.WriteLine("Iscore = {0} and Fscore = {1}%", Iscore, Fscore);
            TotalScore = Fscore;
            

            return TotalScore;
        }

        // ------------------------------------------


        public int NumScore(string title, string tobematched)
        {
            //string tobematched = "word";
            //string title = "count the number of word in a string. count the number of word in a string. count the number of word in a string";
            int count = 0;

            // Check for words
            foreach (Match match in Regex.Matches(title, tobematched, RegexOptions.IgnoreCase))
            {
                count++;
            }
            Console.WriteLine("{0}" + " Found " + "{1}" + " Times", tobematched, count);

            return (count);
        }

        public void ScoreAllTitle(IEnumerable<News> ANews, List<string> KWL)
        {
            foreach (var news in ANews)
            {
                NewsService Ns = new NewsService();
                news.Score = GetTotalScore(news.Title, KWL);
                Ns.Update(news);
            }
        }

        public async void GetHtmlAsync(string[] urls)
        {
            int num = 0;
            foreach (var url in urls)
            {
                num += 1;
                Console.WriteLine($"Site n°{num}");
                Console.WriteLine("################################################");
                ArticleService service = new ArticleService();
                NewsService nservice = new NewsService();

                //Pattern pour la regex
                //string RegPatternUrl = "(?<=www\\.)(.*?)(?=\\.)";
                string RegPatternUrl = "(?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?\\.)+[a-z0-9][a-z0-9-]{0,61}[a-z0-9]";
                //detecter l'url et recupérer le nom du site
                Match m = Regex.Match(url, RegPatternUrl);
                string SiteName = m.Value;

                //recupération des info sur la db et attribution des variable pour la recherche des éléments sur le site
                Article source = service.Get(SiteName);


                //Creation du client http
                var httpClient = new HttpClient();

                //recuperation de la page web
                var html = await httpClient.GetStringAsync(url);

                //creation d'un objet Document Html pour HTMLAgilityPack (permet de innerHtml) et innerText)
                var htmlDocument = new HtmlDocument();

                //chargement de la page dans l'objet
                htmlDocument.LoadHtml(html);

                //recuperation de tous les articles dans une variable
                var Articles = htmlDocument.DocumentNode.Descendants(source.BaliseNameRoot)
                    .Where(node => node.GetAttributeValue(source.AttributeRoot, "")
                    .Equals(source.AttributeNameRoot)).ToList();
                //séparation des articles par item
                var ArticleListItems = Articles[0].Descendants(source.BaliseNameSep)
                    .Where(node => node.GetAttributeValue(source.AttributeSep, "")
                    .Contains(source.AttributeNameSep)).ToList();

                foreach (var article in ArticleListItems)
                {
                    string Ddp;
                    string title;
                    string description;
                    DateTime Dds = DateTime.Now;
                    News news = new News
                    {
                        ScrapeDate = Dds
                    };

                    //afficher seulement les articles par rapport au Keyword

                    //Recuperer la date de parution
                    Console.WriteLine($"temps de parution:");
                    try
                    {
                        Ddp = article.Descendants(source.DdpBalise).Where(node => node.GetAttributeValue(source.DdpAttributeName, "").Equals(source.DdpAttributeValue)).FirstOrDefault().InnerHtml;
                        Console.WriteLine($"{Ddp}");
                        news.Ddp = Ddp;
                    }
                    catch (Exception)
                    {
                        news.Ddp = " ";
                        Console.WriteLine("Pas de date de parution");
                    }



                    //Recupérer le titre
                    Console.WriteLine("Title:");
                    try
                    {
                        if (source.ItemAttribute == " ")
                        {
                            title = article.Descendants(source.ItemBalise).FirstOrDefault().InnerText.Trim('\r', '\n', '\t');
                        }
                        else
                        {
                            title = article.Descendants(source.ItemBalise).Where(node => node.GetAttributeValue(source.ItemAttribute, "").Equals(source.ItemAttributeName)).FirstOrDefault().InnerText.Trim('\r', '\n', '\t');
                        }

                        Console.WriteLine(title);
                        news.Title = title;

                    }
                    catch (Exception)
                    {
                        news.Title = "None";
                        Console.WriteLine($"Title not detected");
                    }

                    if (news.Title != "None")
                    {

                        //Récupérer la description
                        Console.WriteLine("Description:");
                        try
                        {
                            if (source.ItemAttributeDesc == " ")
                            {
                                description = article.Descendants(source.ItemBaliseDesc).LastOrDefault().InnerText.Trim('\r', '\n', '\t');
                            }
                            else
                            {
                                description = article.Descendants(source.ItemBaliseDesc).Where(node => node.GetAttributeValue(source.ItemAttributeDesc, "").Equals(source.ItemAttributeNameDesc)).FirstOrDefault().InnerText.Trim('\r', '\n', '\t');

                            }
                            Console.WriteLine(description);
                            news.Description = description;


                        }
                        catch (Exception)
                        {
                            Console.WriteLine("Pas de description");
                            news.Description = " ";
                        }

                        //Recupérer l'auteur
                        if (source.AuthorBalise != " ")
                        {
                            Console.WriteLine("Auteur:");
                            string Aut;
                            try
                            {
                                Aut = article.Descendants(source.AuthorBalise).Where(node => node.GetAttributeValue(source.AutAttributeName, "").Equals(source.AutAttributeValue)).FirstOrDefault().InnerText;
                                Console.WriteLine(Aut);
                                news.Author = Aut;
                            }
                            catch (Exception e)
                            {
                                news.Author = "None";
                                Console.WriteLine(e.Message);
                                Console.WriteLine("Pas d'auteur");
                            }
                        }
                        else
                        {
                            news.Author = "None";
                        }


                        //récupère le lien vers l'article
                        string RegPatLink = "(?<=href=\")(.+?)\"";
                        string RegDetectFirstCaract = "(^h|^\\/)";
                        string F_Link = "";
                        if (source.LinkAttributeName == " ")
                        {
                            try
                            {
                                F_Link = article.Descendants(source.LinkBalise).FirstOrDefault().InnerHtml.ToString();
                            }
                            catch (Exception)
                            {
                                //afficher un message dans les logs

                            }

                        }
                        else
                        {
                            try
                            {
                                F_Link = article.Descendants(source.LinkBalise)
                                    .Where(node => node.GetAttributeValue(source.LinkAttributeName, "")
                                    .Equals(source.LinkAttributeValue)).FirstOrDefault().InnerHtml;

                            }
                            catch (Exception e)
                            {
                                F_Link = e.Message;

                            }

                        }

                        Match link = Regex.Match(F_Link, RegPatLink);
                        string urlng = link.Value;
                        string urlg = urlng.Replace("\"", "");
                        Match First_Caract = Regex.Match(urlg, RegDetectFirstCaract);
                        Console.WriteLine("Link:");
                        if (urlg == "")
                        {
                            Console.WriteLine("Pas de lien");
                            news.Link = " ";
                        }
                        else if (First_Caract.Value == "h")
                        {
                            Console.WriteLine(urlg);
                            news.Link = urlg;
                        }
                        else if (First_Caract.Value == "/")
                        {
                            Console.WriteLine(source.Url + urlg);
                            news.Link = source.Url + urlg;
                        }
                        Console.WriteLine();
                        if (news.Title == " ")
                        {
                            Console.WriteLine("ERROR: Un probleme est survenu sur l'article");
                        }
                        Console.WriteLine("___________________________________________________________");
                        //insert tous les articles récupérer dans la db
                        news.Id_User = 1;
                        nservice.Insert(news);
                    }                    
                }
            }
        }


        public async void GetHtmlAsync(string[] urls, string keyword, int Id_user)
        {
            int num = 0;
            foreach (var url in urls)
            {
                num += 1;
                Console.WriteLine($"Site n°{num}");
                Console.WriteLine("################################################");
                ArticleService service = new ArticleService();
                NewsService nservice = new NewsService();

                //Pattern pour la regex
                //string RegPatternUrl = "(?<=www\\.)(.*?)(?=\\.)";
                string RegPatternUrl = "(?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?\\.)+[a-z0-9][a-z0-9-]{0,61}[a-z0-9]";
                //detecter l'url et recupérer le nom du site
                Match m = Regex.Match(url, RegPatternUrl);
                string SiteName = m.Value;

                //recupération des info sur la db et attribution des variable pour la recherche des éléments sur le site
                Article source = service.Get(SiteName);


                //Creation du client http
                var httpClient = new HttpClient();

                //recuperation de la page web
                var html = await httpClient.GetStringAsync(url);
               
                //creation d'un objet Document Html pour HTMLAgilityPack (permet de innerHtml) et innerText)
                var htmlDocument = new HtmlDocument();

                //chargement de la page dans l'objet
                htmlDocument.LoadHtml(html);

                //recuperation de tous les articles dans une variable
                var Articles = htmlDocument.DocumentNode.Descendants(source.BaliseNameRoot)
                    .Where(node => node.GetAttributeValue(source.AttributeRoot, "")
                    .Equals(source.AttributeNameRoot)).ToList();
                //séparation des articles par item
                var ArticleListItems = Articles[0].Descendants(source.BaliseNameSep)
                    .Where(node => node.GetAttributeValue(source.AttributeSep, "")
                    .Contains(source.AttributeNameSep)).ToList();

                foreach (var article in ArticleListItems)
                {
                    string Ddp;
                    string title;
                    string description;
                    DateTime Dds = DateTime.Now;
                    News news = new News
                    {
                        ScrapeDate = Dds
                    };
                    
                    //afficher seulement les articles par rapport au Keyword
                    if (article.InnerText.Contains(keyword))
                    {
                        //Recuperer la date de parution
                        Console.WriteLine($"temps de parution:");
                        try
                        {
                            Ddp = article.Descendants(source.DdpBalise).Where(node => node.GetAttributeValue(source.DdpAttributeName, "").Equals(source.DdpAttributeValue)).FirstOrDefault().InnerHtml;
                            Console.WriteLine($"{Ddp}");
                            news.Ddp = Ddp;
                        }
                        catch (Exception)
                        {
                            news.Ddp = " ";
                            Console.WriteLine("Pas de date de parution");
                        }



                        //Recupérer le titre
                        Console.WriteLine("Title:");
                        try
                        {
                            if (source.ItemAttribute == " ")
                            {
                                title = article.Descendants(source.ItemBalise).FirstOrDefault().InnerText.Trim('\r', '\n', '\t');
                            }
                            else
                            {
                                title = article.Descendants(source.ItemBalise).Where(node => node.GetAttributeValue(source.ItemAttribute,"").Equals(source.ItemAttributeName)).FirstOrDefault().InnerText.Trim('\r', '\n', '\t');
                            }
                            
                            Console.WriteLine(title);
                            news.Title = title;

                        }
                        catch (Exception)
                        {
                            news.Title = "None";
                            Console.WriteLine($"Title not detected");
                        }

                        if (news.Title != "None")
                        {

                            //Récupérer la description
                            Console.WriteLine("Description:");
                            try
                            {
                                if (source.ItemAttributeDesc == " ")
                                {
                                    description = article.Descendants(source.ItemBaliseDesc).LastOrDefault().InnerText.Trim('\r', '\n', '\t');
                                }
                                else
                                {
                                    description = article.Descendants(source.ItemBaliseDesc).Where(node => node.GetAttributeValue(source.ItemAttributeDesc, "").Equals(source.ItemAttributeNameDesc)).FirstOrDefault().InnerText.Trim('\r', '\n', '\t');

                                }
                                Console.WriteLine(description);
                                news.Description = description;


                            }
                            catch (Exception)
                            {
                                Console.WriteLine("Pas de description");
                                news.Description = " ";
                            }

                            //Recupérer l'auteur
                            if (source.AuthorBalise != " ")
                            {
                                Console.WriteLine("Auteur:");
                                string Aut;
                                try
                                {
                                    Aut = article.Descendants(source.AuthorBalise).Where(node => node.GetAttributeValue(source.AutAttributeName, "").Equals(source.AutAttributeValue)).FirstOrDefault().InnerText;
                                    Console.WriteLine(Aut);
                                    news.Author = Aut;
                                }
                                catch (Exception e)
                                {
                                    news.Author = "None";
                                    Console.WriteLine(e.Message);
                                    Console.WriteLine("Pas d'auteur");
                                }
                            }
                            else
                            {
                                news.Author = "None";
                            }


                            //récupère le lien vers l'article
                            string RegPatLink = "(?<=href=\")(.+?)\"";
                            string RegDetectFirstCaract = "(^h|^\\/)";
                            string F_Link = "";
                            if (source.LinkAttributeName == " ")
                            {
                                try
                                {
                                    F_Link = article.Descendants(source.LinkBalise).FirstOrDefault().InnerHtml.ToString();
                                }
                                catch (Exception)
                                {
                                    //afficher un message dans les logs

                                }

                            }
                            else
                            {
                                try
                                {
                                    F_Link = article.Descendants(source.LinkBalise)
                                        .Where(node => node.GetAttributeValue(source.LinkAttributeName, "")
                                        .Equals(source.LinkAttributeValue)).FirstOrDefault().InnerHtml;

                                }
                                catch (Exception e)
                                {
                                    F_Link = e.Message;

                                }

                            }

                            Match link = Regex.Match(F_Link, RegPatLink);
                            string urlng = link.Value;
                            string urlg = urlng.Replace("\"", "");
                            Match First_Caract = Regex.Match(urlg, RegDetectFirstCaract);
                            Console.WriteLine("Link:");
                            if (urlg == "")
                            {
                                Console.WriteLine("Pas de lien");
                                news.Link = " ";
                            }
                            else if (First_Caract.Value == "h")
                            {
                                Console.WriteLine(urlg);
                                news.Link = urlg;
                            }
                            else if (First_Caract.Value == "/")
                            {
                                Console.WriteLine(source.Url + urlg);
                                news.Link = source.Url + urlg;
                            }
                            Console.WriteLine();
                            if (news.Title == " ")
                            {
                                Console.WriteLine("ERROR: Un probleme est survenu sur l'article");
                            }
                            Console.WriteLine("___________________________________________________________");
                            //insert tous les articles récupérer dans la db
                            news.Id_User = Id_user;
                            nservice.Insert(news);
                        }


                    }
                }
            }
        }


        public async void GetHtmlAsync(string[] urls, List<string> KWList, int Id_user)
        {
            int num = 0;
            foreach (var url in urls)
            {
                num += 1;
                Console.WriteLine($"Site n°{num}");
                Console.WriteLine("################################################");
                ArticleService service = new ArticleService();
                NewsService nservice = new NewsService();

                //Pattern pour la regex
                //string RegPatternUrl = "(?<=www\\.)(.*?)(?=\\.)";
                string RegPatternUrl = "(?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?\\.)+[a-z0-9][a-z0-9-]{0,61}[a-z0-9]";
                //detecter l'url et recupérer le nom du site
                Match m = Regex.Match(url, RegPatternUrl);
                string SiteName = m.Value;

                //recupération des info sur la db et attribution des variable pour la recherche des éléments sur le site
                Article source = service.Get(SiteName);


                //Creation du client http
                var httpClient = new HttpClient();

                //recuperation de la page web
                var html = await httpClient.GetStringAsync(url);

                //creation d'un objet Document Html pour HTMLAgilityPack (permet de innerHtml) et innerText)
                var htmlDocument = new HtmlDocument();

                //chargement de la page dans l'objet
                htmlDocument.LoadHtml(html);

                //recuperation de tous les articles dans une variable
                var Articles = htmlDocument.DocumentNode.Descendants(source.BaliseNameRoot)
                    .Where(node => node.GetAttributeValue(source.AttributeRoot, "")
                    .Equals(source.AttributeNameRoot)).ToList();
                //séparation des articles par item
                var ArticleListItems = Articles[0].Descendants(source.BaliseNameSep)
                    .Where(node => node.GetAttributeValue(source.AttributeSep, "")
                    .Contains(source.AttributeNameSep)).ToList();

                foreach (var article in ArticleListItems)
                {
                    string Ddp;
                    string title;
                    string description;
                    DateTime Dds = DateTime.Now;
                    News news = new News
                    {
                        ScrapeDate = Dds
                    };

                    foreach (var kw in KWList)
                    {
                        //afficher seulement les articles par rapport au Keyword
                        if (article.InnerText.Contains(kw))
                        {
                            //Recuperer la date de parution
                            Console.WriteLine($"temps de parution:");
                            try
                            {
                                Ddp = article.Descendants(source.DdpBalise).Where(node => node.GetAttributeValue(source.DdpAttributeName, "").Equals(source.DdpAttributeValue)).FirstOrDefault().InnerHtml;
                                Console.WriteLine($"{Ddp}");
                                news.Ddp = Ddp;
                            }
                            catch (Exception)
                            {
                                news.Ddp = " ";
                                Console.WriteLine("Pas de date de parution");
                            }



                            //Recupérer le titre
                            Console.WriteLine("Title:");
                            try
                            {
                                if (source.ItemAttribute == " ")
                                {
                                    title = article.Descendants(source.ItemBalise).FirstOrDefault().InnerText.Trim('\r', '\n', '\t');
                                }
                                else
                                {
                                    title = article.Descendants(source.ItemBalise).Where(node => node.GetAttributeValue(source.ItemAttribute, "").Equals(source.ItemAttributeName)).FirstOrDefault().InnerText.Trim('\r', '\n', '\t');
                                }

                                Console.WriteLine(title);
                                news.Title = title;

                            }
                            catch (Exception)
                            {
                                news.Title = "None";
                                Console.WriteLine($"Title not detected");
                            }

                            if (news.Title != "None")
                            {

                                //Récupérer la description
                                Console.WriteLine("Description:");
                                try
                                {
                                    if (source.ItemAttributeDesc == " ")
                                    {
                                        description = article.Descendants(source.ItemBaliseDesc).LastOrDefault().InnerText.Trim('\r', '\n', '\t');
                                    }
                                    else
                                    {
                                        description = article.Descendants(source.ItemBaliseDesc).Where(node => node.GetAttributeValue(source.ItemAttributeDesc, "").Equals(source.ItemAttributeNameDesc)).FirstOrDefault().InnerText.Trim('\r', '\n', '\t');

                                    }
                                    Console.WriteLine(description);
                                    news.Description = description;


                                }
                                catch (Exception)
                                {
                                    Console.WriteLine("Pas de description");
                                    news.Description = " ";
                                }

                                //Recupérer l'auteur
                                if (source.AuthorBalise != " ")
                                {
                                    Console.WriteLine("Auteur:");
                                    string Aut;
                                    try
                                    {
                                        Aut = article.Descendants(source.AuthorBalise).Where(node => node.GetAttributeValue(source.AutAttributeName, "").Equals(source.AutAttributeValue)).FirstOrDefault().InnerText;
                                        Console.WriteLine(Aut);
                                        news.Author = Aut;
                                    }
                                    catch (Exception e)
                                    {
                                        news.Author = "None";
                                        Console.WriteLine(e.Message);
                                        Console.WriteLine("Pas d'auteur");
                                    }
                                }
                                else
                                {
                                    news.Author = "None";
                                }


                                //récupère le lien vers l'article
                                string RegPatLink = "(?<=href=\")(.+?)\"";
                                string RegDetectFirstCaract = "(^h|^\\/)";
                                string F_Link = "";
                                if (source.LinkAttributeName == " ")
                                {
                                    try
                                    {
                                        F_Link = article.Descendants(source.LinkBalise).FirstOrDefault().InnerHtml.ToString();
                                    }
                                    catch (Exception)
                                    {
                                        //afficher un message dans les logs

                                    }

                                }
                                else
                                {
                                    try
                                    {
                                        F_Link = article.Descendants(source.LinkBalise)
                                            .Where(node => node.GetAttributeValue(source.LinkAttributeName, "")
                                            .Equals(source.LinkAttributeValue)).FirstOrDefault().InnerHtml;

                                    }
                                    catch (Exception e)
                                    {
                                        F_Link = e.Message;

                                    }

                                }

                                Match link = Regex.Match(F_Link, RegPatLink);
                                string urlng = link.Value;
                                string urlg = urlng.Replace("\"", "");
                                Match First_Caract = Regex.Match(urlg, RegDetectFirstCaract);
                                Console.WriteLine("Link:");
                                if (urlg == "")
                                {
                                    Console.WriteLine("Pas de lien");
                                    news.Link = " ";
                                }
                                else if (First_Caract.Value == "h")
                                {
                                    Console.WriteLine(urlg);
                                    news.Link = urlg;
                                }
                                else if (First_Caract.Value == "/")
                                {
                                    Console.WriteLine(source.Url + urlg);
                                    news.Link = source.Url + urlg;
                                }
                                Console.WriteLine();
                                if (news.Title == " ")
                                {
                                    Console.WriteLine("ERROR: Un probleme est survenu sur l'article");
                                }
                                Console.WriteLine("___________________________________________________________");
                                //insert tous les articles récupérer dans la db
                                news.Id_User = Id_user;
                                nservice.Insert(news);
                            }


                        }
                    }


                }
            }
        }
    }
}


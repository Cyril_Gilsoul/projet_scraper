﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using DAL.Entities;
using DAL.Services;

namespace apitest.services
{
    class Score
    { 
        public string sentence { get; set; }
        
        public float GetTotalScore(string sentence, List<string> KWList)
        {            
            float TotalScore = 0;

            // ------------------------------------------

            int Iscore = 0;
            float Fscore = 0;

            // modify title for special characters
            // ...................................
            string str = sentence;
            string newsentence = "";
            char newchar;

            for (int i = 0; i < str.Length; i++)
            {
                // Console.WriteLine(Char.IsLetterOrDigit(str, i));
                newchar = str[i];
                //Console.WriteLine(str[i]);
                if (Char.IsLetterOrDigit(str, i) != true) { newchar = ' '; newsentence += newchar; };
                newsentence += newchar;
                // Console.WriteLine(newsentence);
            }
            Console.WriteLine(newsentence);

            // Proceed to the scoring
            // ......................
            // Keywords toto = new Keywords();
            
            int imax = KWList.Count;
            foreach (var kw in KWList )
            {                
                Iscore += NumScore(newsentence,kw);
                Console.ReadLine();
            }
            Fscore = 100 * (float)Iscore / KWList.Count();
            Console.WriteLine("KList.Count() = {0} ", KWList.Count());
            Console.WriteLine("Iscore = {0} and Fscore = {1}%", Iscore, Fscore);
            Console.ReadLine();

            return TotalScore;
        }

        // ------------------------------------------


        public int NumScore(string title, string tobematched)
        {
            //string tobematched = "word";
            //string title = "count the number of word in a string. count the number of word in a string. count the number of word in a string";
            int count = 0;

            // Check for words
            foreach (Match match in Regex.Matches(title, tobematched, RegexOptions.IgnoreCase))
            {
                count++;
            }
            Console.WriteLine("{0}" + " Found " + "{1}" + " Times", tobematched, count);

            return (count);
        }

        public void ScoreAllTitle(IEnumerable<News>ANews, List<string>KWL)
        {
            foreach (var news in ANews)
            {
                NewsService Ns = new NewsService();
                news.Score = (float)GetTotalScore(news.Title, KWL);
                Ns.Update(news);                
            } 
        }
    }
}

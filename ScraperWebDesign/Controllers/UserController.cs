﻿using DAL.Entities;
using DAL.Services;
using ScraperWebDesign.Mapper;
using ScraperWebDesign.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ScraperWebDesign.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        public ActionResult News()
        {
            // récupération d'une news en utilisant un service de la DAL
            NewsService nservice = new NewsService();
            // on récupère une liste d'objets de la DAL 
            IEnumerable<News> collection = nservice.GetNotValidatedNews();
            // on transforme ces objets en objet en type model            
            IEnumerable<NewsFormModel> model = collection
                .Select(item => item.Map<NewsFormModel>());
            return View(model); // je passe le nom de la vue
        }

        public ActionResult ValidatedNews()
        {
            ViewBag.Message = "Validated News";
            // récupération d'une news en utilisant un service de la DAL
            NewsService nservice = new NewsService();
            // on récupère une liste d'objets de la DAL 
            IEnumerable<News> collection = nservice.GetValidatedNews();
            // on transforme ces objets en objet en type model            
            IEnumerable<NewsFormModel> model = collection
                .Select(item => item.Map<NewsFormModel>());
            return View(model); // je passe le nom de la vue
        }

        public ActionResult Keywords()
        {
            ViewBag.Message = "Keywords";
            return View();
        }

        public ActionResult GoodNews()
        {
            ViewBag.Message = "GoodNews";
            // récupération d'une news en utilisant un service de la DAL
            NewsService nservice = new NewsService();
            // on récupère une liste d'objets de la DAL 
            IEnumerable<News> collection = nservice.GetGoodNews();
            // on transforme ces objets en objet en type model            
            IEnumerable<NewsFormModel> model = collection
                .Select(item => item.Map<NewsFormModel>());
            return View(model); // je passe le nom de la vue
        }

        public ActionResult NewsAlreadyPublished()
        {
            ViewBag.Message = "News Already Published";
            return View();
        }

        public ActionResult DocumentGenerate()
        {
            ViewBag.Message = "Document Generation";
            return View();
        }

        public ActionResult Archives()
        {
            ViewBag.Message = "Archives";
            return View();
        }

        public ActionResult Settings()
        {
            ViewBag.Message = "Settings";
            return View();
        }

        public ActionResult Tampon()
        {
            ViewBag.Message = "Tampon";
            return View();
        }

        public ActionResult Log()
        {
            ViewBag.Message = "Validated News";
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "User Guide";
            return View();
        }

        public ActionResult MyAccount()
        {
            ViewBag.Message = "My Account";
            return View();
        }

        public ActionResult Problems()
        {
            ViewBag.Message = "Problems";
            return View();
        }

    }
}
﻿using ScraperWebDesign.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace ScraperWebDesign.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "About.";
            return View();
        }

        [HttpGet]
        public ActionResult Contact()
        {
            return View(new ContactFormModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Contact(ContactFormModel model)
        {
            if (ModelState.IsValid)
            {
                #region To Do if form is valid
                //on instancie un MailMessage qui contiendra les infos de l'email que l'on souhaite envoyer
                MailMessage message = new MailMessage();
                // on instancie un service qui va envoyer les emails pour nous
                SmtpClient client = new SmtpClient(
                    ConfigurationManager.AppSettings["SMTPServer"]
                );
                // on spécifie les informations de l'email
                message.To.Add(ConfigurationManager.AppSettings["darkchronicle33@hotmail.com"]);
                message.From = new MailAddress(model.AuthorEmail);
                message.Subject = model.Object;
                // on va envoyer un email en html
                message.IsBodyHtml = true;
                message.Body
                    = $"<p>Auteur: {model.AuthorEmail}</p><p>Contenu: {model.Content}</p>";
                // on spécifie la configuration de notre client mail
                client.EnableSsl = true;
                client.Port
                    = int.Parse(ConfigurationManager.AppSettings["SMTPPort"]);
                client.Credentials = new NetworkCredential(
                    ConfigurationManager.AppSettings["SMTPUser"],
                    ConfigurationManager.AppSettings["SMTPPwd"]
                );

                //on envoie l'email
                client.Send(message);

                // TempData ne doit être utilisé que dans le cas d'une redirection
                // sinon utiliser directement le ViewBag
                TempData["SuccessMessage"] = "Votre email a bien été envoyé";

                //rediriger vers la page d'accueil
                return RedirectToAction("Home");
                #endregion
            }
            ViewBag.ErrorMessage = "Le formulaire n'est pas valide";
            return View(model);
        }


        public ActionResult VersionHistory()
        {
            ViewBag.Message = "Version History";
            return View();
        }

        public ActionResult Log()
        {
            ViewBag.Message = "Log";
            return View();
        }
    }
}
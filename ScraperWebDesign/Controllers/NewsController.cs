﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAL.Entities;
using DAL.Services;
using ScraperWebDesign.Mapper;
using ScraperWebDesign.Models;

namespace ScraperWebDesign.Controllers
{
    public class NewsController : Controller
    {
        // GET: News
        public ActionResult Index()
        {
            // récupération d'une news en utilisant un service de la DAL
            NewsService nservice = new NewsService();
            // on récupère une liste d'objets de la DAL 
            IEnumerable<News> collection = nservice.GetAll();
            // on transforme ces objets en objet en type model
            IEnumerable<NewsFormModel> model = collection
                .Select(item => item.Map<NewsFormModel>());
            
            return View(model); // je passe le nom de la vue
        }

        public ActionResult News()
        {
            Scrapper.Scraper Sc = new Scrapper.Scraper();
            //Sc.GetHtmlAsync();
            // récupération d'une news en utilisant un service de la DAL
            NewsService nservice = new NewsService();
            // on récupère une liste d'objets de la DAL 
            IEnumerable<News> collection = nservice.GetAll();
            // on transforme ces objets en objet en type model
            //List<DishDetailsModel> model = new List<DishDetailsModel>();
            //foreach(Dish d in collection)
            //{
            //    model.Add(d.Map<DishDetailsModel>());
            //}
            IEnumerable<NewsFormModel> model = collection
                .Select(item => item.Map<NewsFormModel>());

            return View(model); // je passe le nom de la vue
        }

    }
}
﻿using DAL.Entities;
using DAL.Services;
using ScraperWebDesign.Mapper;
using ScraperWebDesign.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ScraperWebDesign.Controllers
{
    public class SecurityController : Controller
    {
        [HttpGet]
        public ActionResult Register()
        {
            return View(new RegisterFormModel());
        }

        [HttpPost]
        public ActionResult Register(RegisterFormModel model)
        {
            if (ModelState.IsValid)
            {
                // TODO Insérer dans la db
                UserService service = new UserService();
                service.Insert(model.Map<User>());
                // TODO Afficher un message de success
                TempData["SuccessMessage"] = "Votre inscription ...";
                // TODO Rediriger qque part
                return RedirectToAction("index","Home" );
            }
            // TODO Afficher un message d'erreur
            ViewBag.ErrorMessage = "...";
            return View(model);
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View(new LoginFormModel());
        }

        [HttpPost]
        public ActionResult Login(LoginFormModel model)
        {
            if (ModelState.IsValid)
            {
                // Connection au UserService et Récupération d'un user connecté
                UserService service = new UserService();
                User connectedUser = service.Login(model.Email, model.Password);
                // si user != null
                if (connectedUser != null)
                {
                    // Enregistrer l'utilisateur dans une session
                    Utils.Session.Instance.IsLogged = true;
                    Utils.Session.Instance.ConnectedUser = connectedUser;
                    //Utils.Session.Instance.
                    // Redirection + message de bienvenue
                    TempData["SuccessMessage"] = "Bienvenue";
                    return RedirectToAction("index", "Home");
                }
                // Sinon
                // Afficher Message d'erreur et Vue
                ViewBag.ErrorMessage = "Votre mot de passe ou votre email est incorrect";
                return View(model);
            }
            // Afficher un message d'erreur
            ViewBag.ErrorMessage = "Le formulaire n'a pas bien été rempli";
            return View(model);
        }

        public ActionResult Logout()
        {
            Utils.Session.Instance.Logout();
            TempData["SuccessMessage"] = "Au revoir";
            return View();
        }
    }
}
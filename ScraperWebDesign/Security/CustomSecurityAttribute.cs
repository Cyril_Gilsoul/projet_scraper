﻿using DAL.Entities;
using ScraperWebDesign.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ScraperWebDesign.Security
{
    public class CustomSecurityAttribute : AuthorizeAttribute
    {
        // readonly interdit la modification
        //private readonly string[] _AuthorizedRoles;
        private readonly string[] _AuthorizedRoles;

        public CustomSecurityAttribute(params string[] authorizeRoles)
        {
            _AuthorizedRoles = authorizeRoles;
        }

        // Cette méthode sera appelée automatiquement par le system
        // avant l'appel de la méthode du controller
        // que l'on souhaite protégé
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            User connectedUser = Session.Instance.ConnectedUser;
            if (!_AuthorizedRoles.Contains(connectedUser?.Role))
            {
                // refuse l'access et rediriger
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary(new Dictionary<string, object>
                    {
                        { "Controller", "Security" },
                        { "Action", "Login" },
                        { "Area", null }
                    }
                ));
            }
            // sinon
            // ne rien faire
        }
    }
}
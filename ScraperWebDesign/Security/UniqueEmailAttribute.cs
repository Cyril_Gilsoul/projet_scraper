﻿using DAL.Entities;
using DAL.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ScraperWebDesign.Security
{
    public class UniqueEmailAttribute : ValidationAttribute
    {
        public UniqueEmailAttribute(string ErrorMessage = "Cet Email Existe déjà")
        {
            this.ErrorMessage = ErrorMessage;
        }

        // object value = l'email du formulaire
        public override bool IsValid(object value)
        {
            // TODO Vérifier dans la db
            // retourner vrai ou faux
            UserService service = new UserService();
            // récupère un utilisateur d'une liste qui vérifie une condition
            return service.Get((string)value) == null;
        }
    }
}
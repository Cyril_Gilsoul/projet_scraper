﻿using ScraperWebDesign.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ScraperWebDesign.Models
{
    public class RegisterFormModel
    {
        public int Id { get; set; }

        [EmailAddress]
        [Required]
        [MaxLength(255, ErrorMessage = "Le Champs Email est trop long")]
        [UniqueEmail]
        public string Email { get; set; }

        [Required]
        public string Login { get; set; }

        [Required]
        [Compare("ConfirmPassword")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string CondAgreement { get; set; }
    }
}
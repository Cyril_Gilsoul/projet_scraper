﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ScraperWebDesign.Models
{
    public class NewsFormModel
    {
        public int Id {get; set;}
        public string Title {get; set;}
        public string Description {get; set;}
        public string Link {get; set;}
        public string Author {get; set;}
        public string Ddp{get; set;}
        public DateTime ScrapeDate{get; set;}
        public string UserId{get; set;}
        public double Score { get; set; }
    }
}
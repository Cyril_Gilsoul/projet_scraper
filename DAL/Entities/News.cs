﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    public class News
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Author { get; set; }
        public string Ddp { get; set; }
        public string Link { get; set; }
        public DateTime ScrapeDate { get ; set; }
        public int Id_User { get; set; }
        public double Score { get; set; }
    }
}

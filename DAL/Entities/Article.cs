﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    public class Article
    {
        public int Id { get; set; }
        public string SiteName { get; set; }
        public string Url { get; set; }
        public string BaliseNameRoot { get; set; }
        public string AttributeRoot { get; set; }
        public string AttributeNameRoot { get; set; }
        public string BaliseNameSep { get; set; }
        public string AttributeSep { get; set; }
        public string AttributeNameSep { get; set; }
        public string ItemBalise { get; set; }
        public string ItemAttribute { get; set; }
        public string ItemAttributeName { get; set; }
        public string ItemBaliseDesc { get; set; }
        public string ItemAttributeDesc { get; set; }
        public string ItemAttributeNameDesc { get; set; }
        public string DdpBalise { get; set; }
        public string DdpAttributeName { get; set; }
        public string DdpAttributeValue { get; set; }
        public string AuthorBalise { get; set; }
        public string AutAttributeName { get; set; }
        public string AutAttributeValue { get; set; }
        public string Pagination { get; set; }
        public string LinkBalise { get; set; }
        public string LinkAttributeName { get; set; }
        public string LinkAttributeValue { get; set; }        
    }
}

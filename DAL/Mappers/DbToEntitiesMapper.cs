﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Mappers
{
    public class DbToEntitiesMapper
    {
        public static Article ToArticle(IDataReader reader)
        {
            return new Article
            {
                Id = (int)reader["Id"],
                SiteName = reader["SiteName"] as string,
                Url = reader["Url"] as string,
                BaliseNameRoot = reader["BaliseNameRoot"] as string,
                AttributeRoot = reader["AttributeRoot"] as string,
                AttributeNameRoot = reader["AttributeNameRoot"] as string,
                BaliseNameSep = reader["BaliseNameSep"] as string,
                AttributeSep = reader["AttributeSep"] as string,
                AttributeNameSep = reader["AttributeNameSep"] as string,
                ItemBalise = reader["ItemBalise"] as string,
                ItemAttribute = reader["ItemAttribute"] as string,
                ItemAttributeName = reader["ItemAttributeName"] as string,
                ItemBaliseDesc = reader["ItemBaliseDesc"] as string,
                ItemAttributeDesc = reader["ItemAttributeDesc"] as string,
                ItemAttributeNameDesc = reader["ItemAttributeNameDesc"] as string,
                DdpBalise = reader["DdpBalise"] as string,
                DdpAttributeName = reader["DdpAttributeName"] as string,
                DdpAttributeValue = reader["DdpAttributeValue"] as string,
                AuthorBalise = reader["AuthorBalise"] as string,
                AutAttributeName = reader["AutAttributeName"] as string,
                AutAttributeValue = reader["AutAttributeValue"] as string,
                Pagination = reader["Pagination"] as string,
                LinkBalise = reader["LinkBalise"] as string,
                LinkAttributeName = reader["LinkAttributeName"] as string,
                LinkAttributeValue = reader["LinkAttributeValue"] as string,
            };
        }

        public static News ToNews(IDataReader reader)
        {
            return new News
            {
                Id = (int)reader["Id_Article"],
                Title = reader["Titre"] as string,
                Description = reader["Description"] as string,
                Author = reader["AuthorName"] as string,
                Ddp = reader["DatePublished"] as string,
                ScrapeDate = (DateTime)reader["DateScrape"],
                Link = reader["Article_Url"] as string,
                Id_User = (int)reader["Id_User"],
                Score = (double)reader["Score"],
            };
        }

        public static User ToUser(IDataReader reader)
        {
            return new User
            {
                Id = (int)reader["Id_UserProfile"],
                Email = (string)reader["Email"],
                Role = (string)reader["Role"],
                LastName = (string)reader["LastName"],
                FirstName = (string)reader["FirstName"]                
            };
        }

    }
}

﻿using DAL.Entities;
using DAL.Mappers;
using DAL.Toolbox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Services
{
    public class UserService : BaseService<User>
    {
        public override bool Delete(int id)
        {
            Command cmd = new Command("DELETE FROM [UserProfile] WHERE [Id_UserProfile] = @id");
            cmd.AddParameter("@id", id);
            return _Connection.ExecuteNonQuery(cmd) == 1;
        }

        public override User Get(string email)
        {
            Command cmd = new Command("SELECT * FROM [UserProfile] WHERE [Email] = @email");
            cmd.AddParameter("@email", email);
            return _Connection
                .ExecuteReader(cmd, DbToEntitiesMapper.ToUser)
                .FirstOrDefault();
        }

        public override IEnumerable<User> GetAll()
        {
            Command cmd = new Command("SELECT * FROM [UserProfile]");
            return _Connection.ExecuteReader(cmd, DbToEntitiesMapper.ToUser);
        }

        public override int Insert(User item)
        {
            Command cmd = new Command("Sp_Register", true);
            cmd.AddParameter("@email", item.Email);
            cmd.AddParameter("@login", item.Login);
            cmd.AddParameter("@password", item.Password);
            cmd.AddParameter("@lastname", item.LastName);            
            cmd.AddParameter("@firstname", item.FirstName);
            return _Connection.ExecuteNonQuery(cmd);
        }

        public User Login(string email, string password)
        {
            Command cmd = new Command("Sp_Login", true);
            cmd.AddParameter("@email", email);
            cmd.AddParameter("@password", password);
            return _Connection
                .ExecuteReader(cmd, DbToEntitiesMapper.ToUser)
                .FirstOrDefault();
        }

        public override bool Update(User item)
        {
            throw new NotImplementedException();
        }
    }
}

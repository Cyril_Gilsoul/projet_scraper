﻿using DAL.Toolbox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Services
{
    public abstract class BaseService<T>
    {
        //string Server_Name = "localhost\\SQLEXPRESS01";        
        //string Initial_Catalog = "ScraperTest";
        readonly string Server_Name = "TECHNOBEL";
        readonly string Initial_Catalog = "DBScraper";
        readonly string User_Id = "sa";
        readonly string Password = "test1234=";


        protected Connection _Connection;


        public BaseService()
        {
            _Connection = new Connection(
                $"Server={Server_Name};Initial Catalog={Initial_Catalog};User Id={User_Id};Password={Password}",
                "System.Data.SqlClient"
                //$"Server={Server_Name};Initial Catalog={Initial_Catalog} ;Integrated Security=SSPI;",
                //"System.Data.SqlClient"
            );
        }

        public abstract T Get(string word);
        public abstract IEnumerable<T> GetAll();
        public abstract int Insert(T item);
        public abstract bool Update(T item);
        public abstract bool Delete(int id);
    }
}

﻿using DAL.Entities;
using DAL.Mappers;
using DAL.Toolbox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Services
{
    public class NewsService : BaseService<News>
    {
        public override bool Delete(int id)
        {
            string query = "DELETE FROM [News] WHERE Id = @id";
            Command cmd = new Command(query);
            cmd.AddParameter("@id", id);
            return _Connection.ExecuteNonQuery(cmd) == 1;
        }

        public override News Get(string sitename)
        {
            Command cmd = new Command("SELECT * FROM [ScrapeInfo] WHERE sitename = @sitename");
            cmd.AddParameter("sitename", sitename);
            return _Connection
                .ExecuteReader(cmd, DbToEntitiesMapper.ToNews)
                .FirstOrDefault();
        }

        public override IEnumerable<News> GetAll()
        {
            string query = "SELECT * FROM Article";
            Command cmd = new Command(query);
            return _Connection.ExecuteReader(cmd, DbToEntitiesMapper.ToNews);
        }

        public IEnumerable<News> GetNotValidatedNews()
        {
            string query = "SELECT * FROM Article WHERE [Validated] = 0";
            Command cmd = new Command(query);
            return _Connection.ExecuteReader(cmd, DbToEntitiesMapper.ToNews);
        }

        public IEnumerable<News> GetValidatedNews()
        {
            string query = "SELECT * FROM Article WHERE [Validated] = 1";
            Command cmd = new Command(query);
            return _Connection.ExecuteReader(cmd, DbToEntitiesMapper.ToNews);
        }

        public IEnumerable<News> GetGoodNews()
        {
            string query = "SELECT * FROM Article WHERE [Score] >= 50";
            Command cmd = new Command(query);
            return _Connection.ExecuteReader(cmd, DbToEntitiesMapper.ToNews);
        }

        public IEnumerable<News> GetAllById(int id)
        {
            string query = "SELECT * FROM Article WHERE [Id_User] = @iduser";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@idUser", id);
            return _Connection.ExecuteReader(cmd, DbToEntitiesMapper.ToNews);
        }

        public override int Insert(News news)
        {
            string query = "INSERT INTO [Article]([Id_User],[Titre], [Description], [DatePublished], [DateScrape], [AuthorName], [Article_Url],[Score]) OUTPUT inserted.[Id_Article]" +
                " VALUES (@idUser,@title, @desc, @DatePublished, @scrapeDate, @Author, @Link, @score)";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@title", news.Title);
            cmd.Parameters.Add("@idUser", news.Id_User);
            cmd.Parameters.Add("@desc", (object)news.Description ?? DBNull.Value);
            cmd.Parameters.Add("@DatePublished", (object)news.Ddp ?? DBNull.Value);
            cmd.Parameters.Add("@scrapeDate", news.ScrapeDate);
            cmd.Parameters.Add("@Author", news.Author);
            cmd.Parameters.Add("@Link", news.Link);
            cmd.Parameters.Add("@score", news.Score);
            return (int)_Connection.ExecuteScalar(cmd);
        }

        public override bool Update(News news)
        {
            string query = "UPDATE [Article] SET [Score] = @score WHERE [Id_Article] = @idarticle";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@score", news.Score);
            cmd.Parameters.Add("@idarticle", news.Id);
            return _Connection.ExecuteNonQuery(cmd) == 1;
        }

        public bool ValidateNews(News news)
        {
            string query = "UPDATE [Article] SET [Validated] = 1 WHERE [Id_Article] = @idarticle";
            Command cmd = new Command(query);            
            cmd.Parameters.Add("@idarticle", news.Id);
            return _Connection.ExecuteNonQuery(cmd) == 1;
        }


    }
}

﻿using DAL.Entities;
using DAL.Toolbox;
using DAL.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Services
{
    public class ArticleService : BaseService<Article>
    {
        public override bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public override Article Get(string sitename)
        {
            Command cmd = new Command("SELECT * FROM [ScrapeInfo] WHERE [SiteName] = @sitename");
            cmd.AddParameter("@sitename", sitename);
            return _Connection
                .ExecuteReader(cmd, DbToEntitiesMapper.ToArticle)
                .FirstOrDefault();
        }

        public override IEnumerable<Article> GetAll()
        {
            throw new NotImplementedException();
        }

        public override int Insert(Article item)
        {
            throw new NotImplementedException();
        }

        public override bool Update(Article item)
        {
            throw new NotImplementedException();
        }
    }
}


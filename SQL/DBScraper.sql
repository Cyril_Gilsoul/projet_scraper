/*Creation de la db*/
CREATE DATABASE [DBScraper]
go

USE [DBScraper]
go

drop table [Article]
go
/*Creation des tables*/
Create table [ScrapeInfo](
	id int identity primary key,
	SiteName nvarchar(300) Unique ,[Url] nvarchar(200),/*Nom du site et url du site (dois contenir le http(s)://www)*/
	BaliseNameRoot nvarchar(20),AttributeRoot nvarchar(50),AttributeNameRoot nvarchar(100),/*Element qui englobe tout les articles*/
	BaliseNameSep nvarchar(20),AttributeSep nvarchar(50),AttributeNameSep nvarchar(100),/*Element qui s�pare les Articles*/
	ItemBalise nvarchar(20),ItemAttribute nvarchar(50),ItemAttributeName nvarchar(100),/*Element qui designe le titre*/
	ItemBaliseDesc nvarchar(20),ItemAttributeDesc nvarchar(50),ItemAttributeNameDesc nvarchar(100),/*Element qui designe la description*/
	DdpBalise nvarchar(50),DdpAttributeName nvarchar(20),DdpAttributeValue nvarchar(100),/*Element qui designe la date de parution*/
	AuthorBalise nvarchar(10),AutAttributeName nvarchar(20),AutAttributeValue nvarchar(100), /*Element qui designe l'auteur*/
	Pagination nvarchar(20),/*Element qui designe la pagination sans les numero de page*/
	LinkBalise nvarchar(10),LinkAttributeName nvarchar(10),LinkAttributeValue nvarchar(100)/*Element qui designe le lien de l'article*/
)
go

CREATE TABLE [Article]
(
	[Id_Article] INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[Id_User] INT NOT NULL,    
	[Titre] NVARCHAR(max) NOT NULL,
	[Score] FLOAT NOT NULL DEFAULT 0,
	[Context] NVARCHAR(max) NULL,
    [Type] NVARCHAR(50) NULL,
	[Image] NVARCHAR(max) NULL,
    [Validated] BIT NOT NULL default 0,
    [DatePublished] NVARCHAR(MAX) NULL,
    [DateModified] DATETIME2 NULL, 
    [Description] NVARCHAR(max) NULL,
	[DateScrape] DATETIME NOT NULL DEFAULT GetDate(),

	[MainEntityOfPage_Type] NVARCHAR(50) NULL, 
    [MainEntityOfPage_Id] NVARCHAR(50) NULL, 
    
    [AuthorType] NVARCHAR(50) NULL,
    [AuthorName] NVARCHAR(50) NULL,
	[Article_Url] NVARCHAR(max) NULL,
    [PublisherType] NVARCHAR(50) NULL, 
    [OrganizationName] NVARCHAR(50) NULL, 
    [LogoType] NVARCHAR(50) NULL, 
    [LogoURL] NVARCHAR(50) NULL, 
    [HasPartType] NVARCHAR(50) NULL,
	[IsAccessibleForFree] NVARCHAR(50) NULL, 
    [CSS_Selector] NVARCHAR(50) NULL,
	[Id_Url] INT NULL,
	[Id_Domain] INT NULL,
)
go

CREATE TABLE [UserProfile]
(
	[Id_UserProfile] INT NOT NULL IDENTITY(1,1) PRIMARY KEY ,  
    [LastName] NVARCHAR(100) NOT NULL, 
    [FirstName] NVARCHAR(100) NOT NULL, 
    [Login] VARCHAR(50) NOT NULL , 
    [Email] NVARCHAR(255) NOT NULL ,  
	[EmailConfirmed] BIT NOT NULL DEFAULT 0, 
    [PasswordHash] VARBINARY(MAX) NULL,
	[Role] NVARCHAR(50) NOT NULL DEFAULT 'User', 
    [SecurityStamp] NVARCHAR(50) NULL, 
    [PhoneNumber] NVARCHAR(50) NULL, 
    [PhoneNumberConfirmed] BIT NULL DEFAULT 0, 
    [TwoFactorEnabled] BIT NULL, 
    [LockoutEndDateUtc] DATETIME2 NULL, 
    [LockoutEnabled] BIT NULL, 
    [AccessFailedCount] INT NULL, 
    [Id_Domain] INT NULL,    
)
go

CREATE TABLE [dbo].[URLs_List]
(
	[Id_URLs_List] INT NOT NULL PRIMARY KEY, 
    [Id_Domain] INT NOT NULL, 
    [Id_URL] INT NOT NULL, 
)
go

CREATE TABLE [dbo].[URL]
(
	[Id_URL] INT NOT NULL PRIMARY KEY, 
    [ValidityStatus] NVARCHAR(50) NOT NULL, 
    [DateLastVisit] DATETIME2 NULL, 
    [UrlRoot] NVARCHAR(50) NOT NULL, 
    [UrlRobotTxt] NVARCHAR(50) NULL, 
    [UrlSitemap] NVARCHAR(50) NULL, 
	[UrlClasse] NVARCHAR(50) NULL,
	[FeedType] NVARCHAR(50) NULL, 
    [Description] NVARCHAR(50) NULL,
	[Id_Domain] INT NOT NULL,    
)
go

CREATE TABLE [dbo].[Resultat]
(
	[Id_Resultat] INT NOT NULL PRIMARY KEY, 
    [Name] NVARCHAR(50) NOT NULL, 
    [PeriodeDebut] DATETIME2 NOT NULL, 
    [PeriodeFin] DATETIME2 NOT NULL, 
    [Id_UserProfile] INT NOT NULL, 
    [Id_Classe] INT NOT NULL, 
)
go

CREATE TABLE [dbo].[Keywords]
(
	[Id_Keywords] INT NOT NULL PRIMARY KEY, 
    [WordValue] NVARCHAR(50) NOT NULL, 
	[Id_Domain] INT NOT NULL,     
)
go

CREATE TABLE [dbo].[Domain]
(
	[Id_Domain] INT NOT NULL PRIMARY KEY, 
    [Name] NVARCHAR(50) NOT NULL, 
    [Description] NVARCHAR(50) NOT NULL,  
	[Id_Keywords] INT NOT NULL, 
    [Id_URLs_List] INT NULL,
)
go

CREATE TABLE [dbo].[Classe]
(
	[Id_Classe] INT NOT NULL PRIMARY KEY, 
    [Id_Article] INT NOT NULL, 
	[Id_Resultat] INT NOT NULL, 
    [Id_Keywords] INT NOT NULL, 
    [Id_Domain] INT NOT NULL,   
)
go


/*Ajout des constraint*/
ALTER TABLE [dbo].[Article]
ADD CONSTRAINT [FK_Article_ToURL] FOREIGN KEY ([Id_Url]) REFERENCES [URL]([Id_Url]), 
    CONSTRAINT [FK_Article_ToDomain] FOREIGN KEY ([Id_Domain]) REFERENCES [Domain]([Id_Domain])
go

ALTER TABLE [dbo].[URLs_List]
ADD CONSTRAINT [FK_URLs_List_ToDomain] FOREIGN KEY ([Id_Domain]) REFERENCES [Domain]([Id_Domain]), 
    CONSTRAINT [FK_URLs_List_ToURL] FOREIGN KEY ([Id_URL]) REFERENCES [URL]([Id_URL])
go

ALTER TABLE [dbo].[URL]
ADD CONSTRAINT [FK_URL_ToDomain] FOREIGN KEY ([Id_Domain]) REFERENCES [Domain]([Id_Domain])
go

ALTER TABLE [dbo].[Resultat]
ADD CONSTRAINT [FK_Resultat_ToUserProfile] FOREIGN KEY ([Id_UserProfile]) REFERENCES [UserProfile]([Id_UserProfile]), 
    CONSTRAINT [FK_Resultat_ToClasse] FOREIGN KEY ([Id_Classe]) REFERENCES [Classe]([Id_Classe])
go

ALTER TABLE [dbo].[Keywords]
ADD CONSTRAINT [FK_Keyword_ToDomain] FOREIGN KEY ([Id_Domain]) REFERENCES [Domain]([Id_Domain])
go

ALTER TABLE [dbo].[Domain]
ADD CONSTRAINT [FK_Domain_ToURLs_List] FOREIGN KEY ([Id_URLs_List]) REFERENCES [URLs_List]([Id_Urls_List]), 
    CONSTRAINT [FK_Domain_ToKeywords] FOREIGN KEY ([Id_Keywords]) REFERENCES [Keywords]([Id_Keywords])
go

ALTER TABLE [dbo].[Classe]
ADD CONSTRAINT [FK_Classe_ToArticle] FOREIGN KEY ([Id_Article]) REFERENCES [Article]([Id_Article]), 
    CONSTRAINT [FK_Classe_ToResultat] FOREIGN KEY ([Id_Resultat]) REFERENCES [Resultat]([Id_Resultat]), 
    CONSTRAINT [FK_Classe_ToKeywords] FOREIGN KEY ([Id_Keywords]) REFERENCES [Keywords]([Id_Keywords]), 
    CONSTRAINT [FK_Classe_ToDomain] FOREIGN KEY ([Id_Domain]) REFERENCES [Domain]([Id_Domain])
go



/*Drop DB*/
DROP DATABASE [DBScraper]
go
/*Ajout des site compatible */
INSERT INTO [ScrapeInfo](
SiteName,[Url],
BaliseNameRoot,AttributeRoot,AttributeNameRoot,
BaliseNameSep,AttributeSep,AttributeNameSep,
ItemBalise,ItemAttribute,ItemAttributeName,
ItemBaliseDesc,ItemAttributeDesc,ItemAttributeNameDesc,
DdpBalise, DdpAttributeName, DdpAttributeValue,
AuthorBalise, AutAttributeName, AutAttributeValue,
Pagination,
LinkBalise,LinkAttributeName,LinkAttributeValue) 
VALUES (
'www.Lesoir.be','https://www.lesoir.be',
'div','class','list-group media-list gr-direct-list',
'li','class','list-group-item media gr-direct-item',
'h4','class','media-heading gr-direct-item__heading',
'div','class','lead gr-article-teaser',
'time', 'class',' badge gr-direct-item__time',
'p','class','gr-meta gr-meta-author',
'?page=',
'h4','class','media-heading gr-direct-item__heading');
go

INSERT INTO [ScrapeInfo](
SiteName,[Url],
BaliseNameRoot,AttributeRoot,AttributeNameRoot,
BaliseNameSep,AttributeSep,AttributeNameSep,
ItemBalise,ItemAttribute,ItemAttributeName,
ItemBaliseDesc,ItemAttributeDesc,ItemAttributeNameDesc, 
DdpBalise, DdpAttributeName, DdpAttributeValue,
AuthorBalise, AutAttributeName, AutAttributeValue, 
Pagination,
LinkBalise,LinkAttributeName,LinkAttributeValue)
VALUES
('insideevs.com','https://insideevs.com',
'div','class','pre-center',
'div','class','item  wcom',
'div','class','text-box',
'a','class','text',
'span','class','stime',
'span', 'class','author',
' ',
'h3',' ',' ')
go

INSERT INTO [ScrapeInfo](
SiteName,[Url],
BaliseNameRoot,AttributeRoot,AttributeNameRoot,
BaliseNameSep,AttributeSep,AttributeNameSep,
ItemBalise,ItemAttribute,ItemAttributeName,
ItemBaliseDesc,ItemAttributeDesc,ItemAttributeNameDesc, 
DdpBalise, DdpAttributeName, DdpAttributeValue,
AuthorBalise, AutAttributeName, AutAttributeValue, 
Pagination,
LinkBalise,LinkAttributeName,LinkAttributeValue)
VALUES
('insideevs.fr','https://insideevs.fr',
'div','class','pre-center',
'div','class','item  wcom',
'div','class','text-box',
'a','class','text',
'span','class','stime',
'span', 'class','author',
' ',
'h3',' ',' ')
go

INSERT INTO [ScrapeInfo](
SiteName,[Url],
BaliseNameRoot,AttributeRoot,AttributeNameRoot,
BaliseNameSep,AttributeSep,AttributeNameSep,
ItemBalise,ItemAttribute,ItemAttributeName,
ItemBaliseDesc,ItemAttributeDesc,ItemAttributeNameDesc,
DdpBalise, DdpAttributeName, DdpAttributeValue,
AuthorBalise,AutAttributeName,AutAttributeValue,
Pagination,
LinkBalise,LinkAttributeName,LinkAttributeValue) 
VALUES
('us.motorsport.com','https://us.motorsport.com',
'div','class','ms-grid-hor-items-1-2-3-4-5 ms-grid-items--first',
'div','class','ms-item--art',
'h3','class','ms-item_title',
'p','class','ms-item_subheader',
'span','class','ms-item_date-value',
' ',' ',' ',
'?p=',
'h3','class','ms-item_title')
go


INSERT INTO [ScrapeInfo](SiteName,[Url],
BaliseNameRoot,AttributeRoot,AttributeNameRoot,
BaliseNameSep,AttributeSep,AttributeNameSep,
ItemBalise,ItemAttribute,ItemAttributeName,
ItemBaliseDesc,ItemAttributeDesc,ItemAttributeNameDesc,
DdpBalise,DdpAttributeName,DdpAttributeValue,
AuthorBalise,AutAttributeName,AutAttributeValue,
Pagination,
LinkBalise,LinkAttributeName,LinkAttributeValue) 
VALUES
('catalysts.basf.com','https://www.catalysts.basf.com',
'div','class','row news-feed',
'div', 'class','col-xs-12 news-post',
'h2',' ',' ',
'p',' ',' ',
'p','class','lead',
' ',' ',' ',
'p',
'div','class','col-md-9')
go

INSERT INTO [ScrapeInfo](
SiteName,[Url],
BaliseNameRoot,AttributeRoot,AttributeNameRoot,
BaliseNameSep,AttributeSep,AttributeNameSep,
ItemBalise,ItemAttribute,ItemAttributeName,
ItemBaliseDesc,ItemAttributeDesc,ItemAttributeNameDesc,
DdpBalise, DdpAttributeName, DdpAttributeValue,
AuthorBalise,AutAttributeName,AutAttributeValue,
Pagination,
LinkBalise,LinkAttributeName,LinkAttributeValue) 
VALUES
('fr.motorsport.com','https://fr.motorsport.com',
'div','class','ms-grid-hor-items-1-2-3-4-5 ms-grid-items--first',
'div','class','ms-item--art',
'h3','class','ms-item_title',
'p','class','ms-item_subheader',
'span','class','ms-item_date-value',
' ',' ',' ',
'?p=',
'h3','class','ms-item_title')
go






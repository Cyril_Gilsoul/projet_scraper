CREATE PROCEDURE [dbo].[Sp_Login]
	@email NVARCHAR(255),
	@password NVARCHAR(255)
AS
	SELECT * FROM [UserProfile]
	WHERE Email = @email AND [PasswordHash] = dbo.Udf_Hash_Password(@password, @email)
RETURN 0
CREATE PROCEDURE [Sp_Register]
	@email NVARCHAR(255),
	@login VARCHAR(50),
	@password NVARCHAR(255),
	@lastname NVARCHAR(100),
	@firstname NVARCHAR(100)
AS
	INSERT INTO [UserProfile]
	(Email,[Login], [PasswordHash], LastName, FirstName)
	VALUES(
		@email,
		@login,
		dbo.Udf_Hash_Password(@password, @email),
		@lastname,
		@firstname
	)
RETURN 0
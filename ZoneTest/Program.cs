﻿using DAL.Entities;
using DAL.Services;
using Scrapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZoneTest
{
    class Program
    {
        static void Main(string[] args)
        {
            //test du scraper
            string[] urls = new string[] 
            {
                "https://insideevs.fr/news/",
                "https://insideevs.com/news/",
                "https://www.lesoir.be/81857/sections/fil-info",
                "https://us.motorsport.com/all/news/",
                "https://catalysts.basf.com/news/",
                "https://fr.motorsport.com/all/news/"
            };

            Scraper scraper = new Scraper();
            scraper.GetHtmlAsync(urls);
            Console.ReadLine();

            //test de la DAL
            //NewsService service = new NewsService();
            //IEnumerable<News> news = service.GetAll();
            //foreach (var item in news)
            //{
            //    Console.WriteLine(item.Title);
            //    Console.WriteLine(item.Link);
            //}

            //UserService uservice = new UserService();
            //User us = new User
            //{
            //    Login = "Dark",
            //    Email = "Darkchronicle33@hotmail.com",
            //    FirstName = "Cyril",
            //    LastName = "Gilsoul",
            //    Password = "test123",
            //    Role = "Admin"
            //};
            //uservice.Insert(us);
            //Console.WriteLine($"User: {us.Login} Created");
            //uservice.Delete(2);

            Scraper Sc = new Scraper();
            NewsService nservice = new NewsService();
            IEnumerable<News> AllNews;
            AllNews = nservice.GetAll();
            List<string> KWL = new List<string> { "Batterie", "nouvelle", "belge", "politique" };
            Sc.ScoreAllTitle(AllNews, KWL);
            foreach (var item in AllNews)
            {
                Console.WriteLine(item.Id);
                Console.WriteLine(item.Score);
            }
            Console.ReadKey();
        }
    }
}
